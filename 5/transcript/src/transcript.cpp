#include <list>
#include <utility>
#include <algorithm>
#include <string>
#include <iterator>
#include <iostream>
#include <utility>
#include "transcript.hpp"

void addTranscript(std::list<Transcript>& registry, const Transcript& t) {
    registry.push_back(t);
}

void removeTranscript(std::list<Transcript>& registry, const std::string& student_id) {
    remove_if(registry.begin(), registry.end(), 
    [student_id](Transcript T)
    {return T.student_id == student_id;} //Removes if true
    );
}

std::list<Transcript>::const_iterator findTranscript(const std::list<Transcript>& registry, const std::string& student_id) {
    auto it = find_if(registry.begin(), registry.end(),
    [student_id](Transcript T)
    {return T.student_id == student_id;}
    );

    return it;
}

std::list<std::pair<std::string, size_t>> findCourseResults(const std::list<Transcript>& registry, const std::string& course) {
    std::list<std::pair<std::string, size_t>> ret;
    for_each(registry.begin(), registry.end(), //for each student in registry
    [course, &ret] (Transcript T)
    {   
        std::string id = T.student_id; 
        for_each(T.grades.begin(), T.grades.end(), //for each grade a student has
        [course, id ,&ret] (std::pair<std::string, size_t> G)
        {   
            if (G.first == course) {
                std::pair<std::string, size_t> match(id, G.second); //if course code matches make pair of id and grade
                ret.push_back(match);}//push_back id and grade
        }) 
    ;}
    );

    return ret;
}




#include "printers.hpp"

#include <stdexcept>
#include <algorithm>

//StringPrinter implementation starts here 
StringPrinter::StringPrinter(std::ostream& os = std::cout): os(os) {}
StringPrinter::~StringPrinter() = default;
//StringPrinter implementation ends here

//DiagonalPrinter implementatation starts here 
DiagonalPrinter::DiagonalPrinter(std::string first, std::string last, std::ostream &os):
StringPrinter(os), first_(first), last_(last) {} //constructor

DiagonalPrinter* DiagonalPrinter::clone() const{return new DiagonalPrinter("", "", os);}
StringPrinter& DiagonalPrinter::operator() (const std::string& str) {
   if(first_ != "") {os<<first_<<std::endl;}
   
   for (unsigned int i = 0; i < str.size(); i++) {
       std::string empty_string(str.size()-1, ' '); //make empty string
       empty_string.insert(i, 1, str.at(i)); //insert ith letter of str ith position in empty_string once
       os<<empty_string<<std::endl;
   }

   if(last_ != "") {os<<last_;} 

   return *this;
}
//DiagonalPrinter implementation ends here

//Printers implementation starts here 
Printers::Printers() {}

Printers::Printers(const Printers&) = default;

Printers::~Printers() = default;

void Printers::add(const std::string& str, StringPrinter* printer) {
     
    auto match = index.find(str);

    if (match != index.end()) {throw std::invalid_argument("Duplicate index");} //duplicate

    else if (printer == nullptr) {throw std::invalid_argument("Invalid printer");} //pointer is NULL

    else {index.insert( std::pair<std::string, StringPrinter*> (str, printer));} //valid arguments -> insert to map
}

StringPrinter& Printers::operator[](const std::string& str) {
    auto match = index.find(str);

    if (match == index.end()) {throw std::invalid_argument("Unknown index");} //not found 
    
    else {return (*(match->second));} //valid argument -> return ref to StringPrinter       
}

Printers& Printers::operator=(const Printers&) = default;
//Printers implementation ends here

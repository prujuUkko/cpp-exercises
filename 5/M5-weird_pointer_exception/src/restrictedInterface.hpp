#pragma once
#include "restrictedPtrEx.hpp"


template<typename T>
WeirdMemoryAllocator::restrictedPtr<T> makeRestricted(T* ptr)
{
	// implement the makeRestricted interface here
	try {
		WeirdMemoryAllocator::restrictedPtr<T> new_ptr(ptr, "restrictedptr instance");
		return new_ptr;
	} catch (WeirdMemoryAllocator::restrictedPtrEx &e) {
	    std::cout << e.getError() << std::endl;
		WeirdMemoryAllocator::restrictedPtr<T> fail_ptr(nullptr, "nullptr");
		return fail_ptr;
	}
}

template<typename T>
WeirdMemoryAllocator::restrictedPtr<T> copyRestricted(WeirdMemoryAllocator::restrictedPtr<T>& w_restrPtr)
{
	// implement the copyRestricted interface here
	try {
		WeirdMemoryAllocator::restrictedPtr<T>& new_ptr = w_restrPtr;
		return new_ptr;
	} catch (WeirdMemoryAllocator::restrictedPtrEx &e) {
	    std::cout << e.getError() << std::endl;
		WeirdMemoryAllocator::restrictedPtr<T> fail_ptr(nullptr, "nullptr");
		return fail_ptr;
	}
}

template<typename T>
void printRestricted(WeirdMemoryAllocator::restrictedPtr<T>& w_restrPtr)
{
	// implement the printRestricted interface here
	// both exception and value printing should end in newline
	try {
		std::cout<<w_restrPtr.getData()<<std::endl;
	} catch (WeirdMemoryAllocator::restrictedPtrEx &e) {
	    std::cout << e.getError() << std::endl;
	}
}
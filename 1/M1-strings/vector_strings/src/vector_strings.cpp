#include <iostream>
#include <vector>
#include "vector_strings.hpp"

void adder(std::vector<std::string>& names)
{
    std::cout << "Enter a name:" << std::endl;
    std::string name;
    std::cin >> name;
    names.push_back(name);
    std::cout << "Number of names in the vector:" << std::endl;
    std::cout << names.size() << std::endl;
}

void remover(std::vector<std::string>& names)
{
    std::cout << "Removing the last element:" << std::endl;
    std::string removed = names[names.size()-1];
    std::cout << removed << std::endl;
    names.pop_back();
}

void printer(std::vector<std::string>& names)
{
    for (unsigned int i = 0; i < names.size(); i++){
        std::cout << names[i] << std::endl;
        }
}

void cmdReader()
{
    std::cout << "Commands: ADD, PRINT, REMOVE, QUIT" << std::endl;
	  std::vector<std::string> names;

	while (1) {
	    std::cout << "Enter command:" << std::endl;
        std::string command;
        std::cin >> command;

        if (command == "ADD") {
            adder(names);
        }

        else if (command == "REMOVE") {
            remover(names);
        }

        else if (command == "PRINT") {
            printer(names);
        }

        else if (command == "QUIT") {
            break;
        }
	}
}

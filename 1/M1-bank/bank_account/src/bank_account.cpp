#include "bank_account.hpp"

#include <iostream>

/* BankAccount:
 * the constructor of the BankAccount class, takes the following parameters:
 * the account's owner (string const&),
 * the account's number (string const&)
 * and the account's balance (double).
 */
BankAccount::BankAccount(std::string const& Owner, std::string const& AccountNumber, double Balance) {
  owner = Owner;
  accountNumber = AccountNumber;
  balance = Balance;
}

/* getOwner:
 * returns the BankAccount's owner as a string, takes no parameters.
 */
std::string BankAccount::getOwner() const {
  return owner;
}

/* getNumber:
 * returns the BankAccount's number as a string, takes no parameters.
 */
std::string BankAccount::getNumber() const {
  return accountNumber;
}

/* getBalance:
 * returns the BankAccount's balance as a double, takes no parameters.
 */
double BankAccount::getBalance() const {
  return balance;
}

/* deposit:
 * deposits (stores) the parameter amount <i>(double)</i> to the BankAccount, returns nothing.
 */
void BankAccount::deposit(double amount) {
  balance += amount;
}

/* withdraw:
 * withdraws the parameter amount from the BankAccount.
 * If the parameter amount is bigger than the BankAccount's current balance,
 * no withdraw is made and the function returns false.
 * If the amount is smaller or equal to the current balance
 * the balance is reduced by the parameter amount and the function returns true.
 */
bool BankAccount::withdraw(double amount) {
  if (amount > balance) {
    return false;
  }
  else {
    balance -= amount;
    return true;
  }
}

/* print:
 * prints the account information to the standard output stream.
 * The function takes no parameters and returns nothing.
 * The output format should be like the following:
 Account num: <accountNumber>, owned by: <owner>, balance: <balance> eur.\n
 */
void BankAccount::print() const {
  std::cout << "Account num: " << getNumber() << ", owned by: " << getOwner() << ", balance: " << getBalance() << " eur." << std::endl;
}

/* transfer:
 * transfers money from the source account to the target account.
 * If the parameter amount is bigger than the current balance of the source account,
 * the function returns false and no transfer is made.
 * If the amount is valid, that is smaller than the source account's current balance,
 * the function adds the parameter amount to the target account's balance,
 * reduces the amount from the source account's balance and returns true.
 */
bool transfer(BankAccount& sourceAccount, BankAccount& targetAccount, double amount) {
  if (amount > sourceAccount.getBalance()) {
    return false;
  }
  else {
    sourceAccount.withdraw(amount);
    targetAccount.deposit(amount);
    return true;
  }
}

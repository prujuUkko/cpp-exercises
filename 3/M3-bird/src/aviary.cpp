#include "bird.hpp"
#include <string>
#include <vector>
#include <algorithm>
#include <functional>
#include <iostream>


Aviary::Aviary() {
    std::vector<Bird*> birds_;    
}

void Aviary::addBird(Bird *b) {
    birds_.push_back(b);
}

Aviary::~Aviary() {
    for (auto i : birds_) {
        delete i;
    }
}


void Aviary::speakAll(std::ostream &os) const {
    for (auto i : birds_) {
        i->speak(os);
    }
}

size_t Aviary::size() const {
    return birds_.size();
}

Bird* Aviary::operator [] (int i) const {
    return birds_[i];
}



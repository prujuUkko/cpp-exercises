#include "poly.hpp"
#include "iostream"
#include <sstream>

int Poly::operator[](int exp) const {
    Values::const_iterator it = m_values.find(exp);
    return it == m_values.end() ? 0 : it->second;
}

Poly& Poly::operator+=(Poly const& b) {
    // TODO: Implement this
    *this = *this + b;
    return *this;
}

Poly& Poly::operator-=(Poly const& b) {
    // TODO: implement this
    *this = *this - b; 
    return *this;
}

std::istream& operator>>(std::istream& is, Poly& p) {
    std::string token;
    if (!(is >> token)) return is; // Couldn't extract the token?
    std::istringstream iss(token);
    Poly tmp; // A temporary in order to avoid modifying p if an error occurs
    bool fail = false;
    for (int m, exp; iss >> m; tmp[exp] += m) {
        if (iss.get() == 'x' && iss >> exp) continue; // All OK, add to tmp
        fail = true;
        break;
    }
    if (!iss.eof()) fail = true; // Fail if iss >> m failed due to garbage rather than EOF
    if (fail) is.clear((is.rdstate() & ~std::ios::goodbit) | std::ios::failbit); // Set iostate fail
    else p = tmp; // All OK, output the result
    return is;
}

std::ostream& operator<<(std::ostream& os, Poly const& p) {
    // TODO: implement this
    for (auto i = p.begin(); i != p.end(); i++) {
        os << (*i).second << "x" << (*i).first;
    }

    return os;
}

Poly operator+(Poly const& a, Poly const& b) {
    // TODO: implement this
    Poly ret;

    for (auto i1 = a.begin(); i1 != a.end(); i1++)  {
        ret[(*i1).first] = (*i1).second;  
    }

    for (auto i2 = b.begin(); i2 != b.end(); i2++) {
        ret[(*i2).first] = ret[(*i2).first] + (*i2).second;
    }

    return ret; 
}

Poly operator-(Poly const& a, Poly const& b) {
    // TODO: implement this
    Poly ret;

    for (auto i1 = a.begin(); i1 != a.end(); i1++)  {
        ret[(*i1).first] = (*i1).second;  
    }

    for (auto i2 = b.begin(); i2 != b.end(); i2++) {
        ret[(*i2).first] = ret[(*i2).first] - (*i2).second;
    }

    return ret;
}

Poly operator-(Poly const& p) {
    // TODO: implement this
    Poly new_p;
    new_p = p;

    for (auto i = new_p.begin(); i != new_p.end(); i++) {
        (*i).second = -((*i).second);
    } 

    return new_p;
}

bool operator<(Poly const& a, Poly const& b) {
    // TODO: implement this
    if ((*a.begin()).first < (*b.begin()).first) {
        return true;
    }

    return false;
}

bool operator==(Poly const& a, Poly const& b) {
    // TODO: implement this
    if ((*a.begin()).first == (*b.begin()).first) {
        return true;
    }

    return false;
}

bool operator>(Poly const& a, Poly const& b) {
    // TODO: implement this
    if ((*a.begin()).first > (*b.begin()).first) {
        return true;
    }

    return false;
}

bool operator!=(Poly const& a, Poly const& b) {
    // TODO: implement this
    if ((*a.begin()).first != (*b.begin()).first) {
        return true;
    }

    return false;
}

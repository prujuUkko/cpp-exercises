#include <string>
#include <vector>



class Vehicle {
public:
	explicit Vehicle(std::string w_regNum, std::string w_owner);
	virtual ~Vehicle();
	
	// simple getter, returns this instance's register number
	std::string getRegNum() const;

	// simple getter, returns this instance's owner
	std::string getOwner() const;

	// pure virtual functions, you should implement these in the child classes!
	virtual void regWriter(std::ofstream& w_outStream) = 0;

	// this function should print out the instance to the standard output (cout)
	// The format should be exactly the same as in the file read/write serialization.
	virtual void printVehicle() = 0;

private:
	std::string m_regNum;
	std::string m_owner;
}; // end class Vehicle


// this class is just a simple container for an array (vector) of Vehicle instances
class Register {
public:
	Register();
	~Register();

	// adds an vehicle to the register. Takes a pointer to a dynamically allocated instance as param
	void addVehicle(Vehicle* v);

	// serializes the register instances into a file
	void outputAll(const std::string& w_filename) const;

	// this function is called per line in the file that is being read
	bool readOneLine(std::istream& w_inStream);

	// opens a file and handles IO operations, calls readOneLine for every line read from the file
	int readToRegister(const std::string& w_filename);

	// prints the register to standard output
	// CAUTION: you should use the same formatting here as when writing to
	// the text file.
	void printRegister();

	// returns the register size
	size_t size() const;
private:
	// this container only sees that Vehicle instances are stored. Actually these instances are boats, cars...
	std::vector<Vehicle *> m_vehicles;
}; // end class Register


/*
 * Implement here classes Aircraft, Car and Boat
 * All of these classes should inherit from Vehicle.
 */

class Aircraft: public Vehicle {
    public:
	Aircraft(std::string w_regNum, std::string w_owner, std::string model, double wing_span, unsigned int cruise_speed);    
    
	std::string getModel() const;
	double getWing_span() const;
	unsigned int getCruise_speed() const;
    
	static Aircraft* readRest(std::istream& w_inStream);

	virtual void regWriter(std::ofstream& w_outStream);
	virtual void printVehicle();

	private:
	std::string model_;
	double wing_span_;
	unsigned int cruise_speed_;
};

class Car: public Vehicle {
    public:
	Car(std::string w_regNum, std::string w_owner, std::string maker, unsigned int range, bool inspection);

    std::string getMaker();
	unsigned int getRange();
	bool getInspection();

	static Car* readRest(std::istream& w_inStream);

	virtual void regWriter(std::ofstream& w_outStream);
	virtual void printVehicle();

	private:
	std::string maker_;
	unsigned int range_;
	bool inspection_;
};

class Boat: public Vehicle {
    public:
	Boat(std::string w_regNum, std::string w_owner, std::string name, double draft, double power); 
    
    std::string getName();
	double getDraft();
	double getPower();

    static Boat* readRest(std::istream& w_inStream);

	virtual void regWriter(std::ofstream& w_outStream);
	virtual void printVehicle();
    
	private:
	std::string name_;
	double draft_;
	double power_;
};
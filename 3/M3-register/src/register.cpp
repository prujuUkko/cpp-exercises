#include "register.hpp"
#include <fstream>
#include <iostream>
#include <sstream>

/*
 * VEHICLE CLASS FUNCTIONS
 */
Vehicle::Vehicle(std::string w_regNum, std::string w_owner)
	:
	m_regNum(w_regNum),
	m_owner(w_owner)
{
}

Vehicle::~Vehicle() {}

std::string Vehicle::getRegNum() const
{
	return m_regNum;
}

std::string Vehicle::getOwner() const
{
	return m_owner;
}

/*
 * REGISTER CLASS FUNCTIONS
 */
Register::Register() {}

Register::~Register()
{
	for(auto i = m_vehicles.begin(); i != m_vehicles.end(); i++)
	{
		delete *i;
	}
}

void Register::addVehicle(Vehicle* v)
{
	m_vehicles.push_back(v);
}

void Register::outputAll(const std::string& w_filename) const
{
	// write here a function that serializes the Vehicle instances in the correct format
	// to output file (filename given as parameter)
	// instances are separated by newline

	// FORMAT :
	// case Aircraft:
	// A;regnum;owner;model;wingspan;cruisespeed
	//
	// case Boat:
	// B;regnum;owner;name;draft;power
	// 
	// case Car:
	// C;regnum;owner;maker;range;inspection
    
	std::ofstream os(w_filename);
	for (auto i : m_vehicles) {
		i->regWriter(os);
	}
}

void Register::printRegister()
{
	for(auto i = m_vehicles.begin(); i != m_vehicles.end(); i++)
	{
		(*i)->printVehicle();
	}
}

bool Register::readOneLine(std::istream& w_inStream)
{
	// TODO:
	// take the istream from the readToRegister function, and check the
	// current type of vehicle. Call the readRest function with the current istream as parameter, once again
	// (we are learning the iostreams here) of the corresponding
	// vehicle type and add the returned new instance to register.
    Vehicle* new_vehicle;
	std::string line;
	std::getline(w_inStream, line);
    char type = line.at(0);
    std::stringstream ss(line);
	
	if (type == 'B') {
		new_vehicle = Boat::readRest(ss);
	}

	else if (type == 'A') {
        new_vehicle = Aircraft::readRest(ss);
	}

	else if (type == 'C') {
        new_vehicle = Car::readRest(ss);
	}

	else {
		return false;
	}

	Register::addVehicle(new_vehicle);
	return true;

}

int Register::readToRegister(const std::string& w_filename)
{
	// TODO:
	// open the file (filename given as param) and read file line by line.
	// give the istream to readOneLine from each line.
	// return: the number of vehicles added. If there was an error reading the file,
	// -1 should be returned.

	std::ifstream is(w_filename);

    if (is.rdstate() & (is.failbit | is.badbit)) { 
		return -1; //error in reading file
    }
	
	int counter = 0;
	
	//Empty file: https://stackoverflow.com/questions/26228424/how-to-detect-an-empty-file-in-c/26228684
	if ( is.peek() == std::ifstream::traits_type::eof() ) {
		return counter; 
	}

    std::string line;

	while (!is.eof()) {
		std::getline(is, line);
        std::stringstream ss(line);

		if(Register::readOneLine(ss)) {
			counter++;
		}
	}
	return counter;

}

size_t Register::size() const
{
	return m_vehicles.size();
}

/*
 * Implement here classes Aircraft, Car and Boat
 * All of these classes should inherit from Vehicle.
 */

//AIRCRAFT FUNCTION IMPLEMENTATIONS START HERE
Aircraft::Aircraft(std::string w_regNum, std::string w_owner, std::string model, double wing_span, unsigned int cruise_speed): 
Vehicle(w_regNum, w_owner), model_(model), wing_span_(wing_span), cruise_speed_(cruise_speed) {}

std::string Aircraft::getModel() const {return Aircraft::model_;}
double Aircraft::getWing_span() const {return Aircraft::wing_span_;}
unsigned int Aircraft::getCruise_speed() const {return Aircraft::cruise_speed_;}

Aircraft* Aircraft::readRest(std::istream& w_inStream) {
	std::string item;
	std::vector<std::string> va;

	while(std::getline(w_inStream, item, ';')) {
		va.push_back(item);
	}
	//Now: va[0..5] == "A", regnum, owner, model, wingspan, cruisespeed
	//stod = string to double, stoul = string to unsigned long  
	Aircraft* new_a = new Aircraft(va[1], va[2], va[3], std::stod(va[4]), std::stoul(va[5]));
	
	return new_a;
}

void Aircraft::regWriter(std::ofstream& w_outStream) {
    w_outStream << "A;" << Aircraft::getRegNum() << ";" <<  Aircraft::getOwner() << ";" << Aircraft::getModel() 
	<< ";" << Aircraft::getWing_span() << ";" << Aircraft::getCruise_speed() << std::endl;     
}

void Aircraft::printVehicle() {
	std::cout << "A;" << Aircraft::getRegNum() << ";" <<  Aircraft::getOwner() << ";" << Aircraft::getModel() 
	<< ";" << Aircraft::getWing_span() << ";" << Aircraft::getCruise_speed() << std::endl;          
}  

//AIRCRAFT FUNCTION IMPLEMENTATIONS END HERE

//CAR FUNCTION IMPLEMENTATIONS START HERE
Car::Car(std::string w_regNum, std::string w_owner, std::string maker, unsigned int range, bool inspection): 
Vehicle(w_regNum, w_owner), maker_(maker), range_(range), inspection_(inspection) {}
    
std::string Car::getMaker() {return Car::maker_;}
unsigned int Car::getRange() {return Car::range_;}
bool Car::getInspection() {return Car::inspection_;}

Car* Car::readRest(std::istream& w_inStream) {
	std::string item;
	std::vector<std::string> vc;
	while(std::getline(w_inStream, item, ';')) {
        vc.push_back(item);
	}
	//Now: vc[0..5] == "C", regnum, owner, maker, range, inspection
	bool inspection;
	
	if(vc[5] == "1") {inspection = true;}
	else {inspection = false;} 
    
	//stoul = string to unsigned long 
	Car* new_c = new Car(vc[1], vc[2], vc[3], std::stoul(vc[4]), inspection);
	
	return new_c;
}

void Car::regWriter(std::ofstream& w_outStream) {
	w_outStream << "C;" << Car::getRegNum() << ";" <<  Car::getOwner() << ";" << Car::getMaker() << ";" << Car::getRange()
	<< ";" << Car::getInspection() << std::endl;
}
void Car::printVehicle() {
	std::cout << "C;" << Car::getRegNum() << ";" <<  Car::getOwner() << ";" << Car::getMaker() << ";" << Car::getRange()
	<< ";" << Car::getInspection() << std::endl;
}
//CAR FUNCTION IMPLEMENTATIONS END HERE


//BOAT FUNCTION IMPLEMENTATIONS START HERE
Boat::Boat(std::string w_regNum, std::string w_owner, std::string name, double draft, double power): 
Vehicle(w_regNum, w_owner), name_(name), draft_(draft), power_(power) {}
    
std::string Boat::getName() {return Boat::name_;}
double Boat::getDraft() {return Boat::draft_;}
double Boat::getPower() {return Boat::power_;}

Boat* Boat::readRest(std::istream& w_inStream) {
	std::string item;
	std::vector<std::string> vb;
	while(std::getline(w_inStream, item, ';')) {
        vb.push_back(item);
	}
	//Now: vb[0..5] == "B", regnum, owner, name, draft, power
	//stod = string to double 
	Boat* new_b = new Boat(vb[1], vb[2], vb[3], std::stod(vb[4]), std::stod(vb[5]));
	
	return new_b;
}

void Boat::regWriter(std::ofstream& w_outStream) {
	w_outStream << "B;" << Boat::getRegNum() << ";" << Boat::getOwner() << ";" << Boat::getName() << ";" << Boat::getDraft()
	<< ";" << Boat::getPower() << std::endl;
}
void Boat::printVehicle() {
	std::cout << "B;" << Boat::getRegNum() << ";" << Boat::getOwner() << ";" << Boat::getName() << ";" << Boat::getDraft()
	<< ";" << Boat::getPower() << std::endl;
}
//BOAT FUNCTION IMPLEMENTATIONS END HERE

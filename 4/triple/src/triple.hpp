/* Add include guards here */
#ifndef TRIPLE_H
#define TRIPLE_H


/* Class: Triple
 * -------------
 * The template class Triple is a container class, which holds three values.
 * All of the values can be of different type. All of the values should be
 * private.
 *
 * The constructor of the class takes three const references to values as a
 * parameter. The parameters are in order, in other words, after construction
 * the first parameter should be accessible through the public member function
 * first, the second parameter through the public member function, second, and
 * the third through the public member function third. 
 *
 * The default empty constructor should also work.
 *
 * The Triple class should also support the copy constructor, and assignment
 * operations.
 */
template <typename T1, typename T2, typename T3> class Triple {

/* Public member functions:
 * ------------------------
 * The triple class has three public member functions, which are used to
 * access the stored elements.
 *
 * Function: first
 * The function takes no parameters.
 * The function returns a const reference to the first value of the triple.
 *
 * The function takes no parameters.
 * The function returns a const reference to the second value of the triple.
 *
 * Function: third
 * The function takes no parameters.
 * The function returns a const reference to the third value of the triple.
 */

    public:
    Triple(T1 const& first, T2 const& second, T3 const& third):
    first_(first), second_(second), third_(third) {}; //Parametrized constructor
    Triple(); //Default constructor 
    Triple(const Triple& a) = default; // Default copy constructor 
    Triple &operator=(const Triple&) = default; //Default assingment constructor 
    T1 const& first() const {return first_;}
    T2 const& second() const {return second_;}
    T3 const& third() const {return third_;}
    
    private:
    T1 first_;
    T2 second_;
    T3 third_;
};

/* Non-member functions:
 * ---------------------
 * Function: isHomogenous
 * The function is a non-member function that takes a triple as a parameter.
 * The function returns a boolean value indicating whether the triple is
 * homogenous or not. A triple is homogenous if all of its three values are of
 * the same type.
 *
 * Hint: specialisation can help you here.
 *
 * Function: 
 * The function is a non-member function that takes three arbitrary type values
 * (note: values, not references) as a parameter and returns a triple created
 * from those three values.
 */

template <typename T1, typename T2, typename T3> bool isHomogenous(Triple <T1, T2, T3>) {
    return false; // 3 different typenames
}

template <typename T1> bool isHomogenous(Triple <T1, T1, T1>) {
    return true; // 3 same typenames
}

template <typename T1, typename T2, typename T3> Triple<T1, T2, T3> makeTriple(T1 first, T2 second, T3 third) {
    Triple<T1, T2, T3> ret(first, second, third);
    return ret; 
}

/* Operator overloads:
 * -------------------
 * The triple class should support the following operators:
 *
 * Assignment operator, =
 * The default functionality should do here.
 *
 * Equality operator, ==
 * You can assume that all the values in the triple can be compared by the
 * == -operator. Two triples are equal if the corresponding values in both
 * of the triples are equal.
 *
 * Inequality operator, !=
 * You can assume that all the values in the triple can be compared by the
 * != -operator. Two triples are inequal if even one corresponding value in
 * the triples is different.
 *
 * The output stream operator, <<
 * You can assume that all the values in the triple support the << -operator.
 * The triple should be printed to the output stream in the following format: 
 *
 * [<first value>, <second value>, <third value>]
 *
 * Note that there is NO new line at the end of the output.
 *
 */

//template template <typename T1, typename T2, typename T3> works for all possible types of Triple

template <typename T1, typename T2, typename T3> 
bool operator ==(Triple<T1, T2, T3> a, Triple<T1, T2, T3>b) {
    if (a.first() == b.first() && a.second() == b.second() && a.third() == b.third()) {
        return true;
    }
    return false;
}

template <typename T1, typename T2, typename T3>
bool operator !=(Triple<T1, T2, T3> a, Triple<T1, T2, T3> b) {
    bool ret = !(a == b);
    return ret;
}

template <typename T1, typename T2, typename T3>
std::ostream& operator <<(std::ostream& os, Triple<T1, T2, T3> a) {
    os<<"["<<a.first()<<", "<<a.second()<<", "<<a.third()<<"]";
    return os; 
}

/* Remember the endif here to close the header guards */

#endif

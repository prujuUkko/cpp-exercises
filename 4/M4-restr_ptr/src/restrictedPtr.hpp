//HEADER GUARDS
#ifndef RESTRICTED_PTR_H
#define RESTRICTED_PTR_H

class RefCount { //seperate class for keeping track of the reference count
public:
    void set(int a) {count_ = a;}
    
    void add() {count_++;}
    
    void substract() {--count_;}

    int getCount() const {return count_;}

private:
    int count_;
};

template <typename T>
class restrictedPtr {
public:
    restrictedPtr() { //default constructor
        pointer_ = 0;
        ref_count_ = new int(1);
    }
    
    restrictedPtr(T* p) { //constructor for when a pointer is given as a parameter
        pointer_ = p;
        ref_count_ = new int(1);
    }

    ~restrictedPtr() { //destructor
        if(getRefCount == 1) {
            delete pointer_;
            delete ref_count_;
        }
    }
    
    restrictedPtr(const restrictedPtr& ptr) { //copy constructor
        if (getRefCount() < 3) {
            pointer_= ptr.getPointer();
            ref_count_= ptr.getRefCount();
            ref_count_-> add();
        } 
        
        else {
            pointer_= 0;
            ref_count_= new RefCount();
            ref_count_-> add();    
        }
    }

    restrictedPtr<T>& operator=(const restrictedPtr& ptr) { //assingment constructor
        if (this != &ptr) {
            if (ref_count_->getCount() == 1) {
                delete pointer_;
                delete ref_count_;
            }
        
            if ((ptr.getRefCount())->getCount()<3) {
                pointer_= ptr.getPointer();
                ref_count_= ptr.getRefCount();
                ref_count_-> add();
            } 
        
            else {
                pointer_= 0;
                ref_count_= new RefCount();
                ref_count_-> add();    
            }
            
        }
        return *this;
    }

    T* getPointer() const {return pointer_;}
    T& getData() const {return *pointer_;}
    int* getRefCountPointer() const {return ref_count_;}
    int& getRefCount() const {return *ref_count_;}

private:
    T* pointer_;
    int* ref_count_;
};

//CLOSE HEADER GUARDS
#endif 

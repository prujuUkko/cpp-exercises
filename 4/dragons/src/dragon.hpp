﻿#ifndef ELEC_AS_CPP_DRAGON_HH
#define ELEC_AS_CPP_DRAGON_HH

#include <string>
#include <list>
#include <iostream>

/* Enumeration types for the different treasures */
enum TreasureType
{
  Jewellery,
  Wisdom,
  Potions
};

/* Implementation of Treasure */
struct Treasure
{
  TreasureType type;
  std::string name;
};

/* Enumeration types for the different foods */
enum FoodType
{
  PeopleFood,
  People,
  Herbs
};

/* Implementation of Food */
struct Food
{
  FoodType type;
  std::string name;
};

class Dragon
{
  /* Description:
   * ------------
   * An abstract class that describes a Dragon. The Fantasy-, Chinese- and MagicDragon
   * classes inherit this class. Each Dragon object has a name (std::string), 
   * age (size_t), size (size_t) and a list of Treasure objects that the Dragon has
   * hoarded.
   *
   * Functions:
   * ------------
   * The constructor takes the Dragon's name (std::string const&), age (size_t) and 
   * size (size_t) as a parameter and intializes them to the parameter values.
   *
   * getName: the function returns a constant reference to the Dragon's name and takes
   * no parameters. This function does not change the state of the Dragon object, i.e.
   * the function should be const.
   *
   * getAge: the function returns the Dragon's age as a size_t and takes no parameters. 
   * This function does not change the state of the Dragon object, i.e. the function 
   * should be const.
   *
   * getSize: the function returns the Dragon's size as a size_t and takes no parameters.
   * This function does not change the state of the Dragon object, i.e. the function 
   * should be const.
   *
   * getTreasures: the function returns a constant reference to the Dragon's treasures list
   * and takes no parameters. This function does not change the state of the Dragon object, 
   * i.e. the function should be const.
   *
   * eat: a pure virtual function, which returns nothing and takes a reference to a list of
   * Food objects as a parameter.
   *
   * hoard: a pure virtual function, which returns nothing and takes a reference to a list of
   * Treasure objects as a parameter.
   *
   * Other:
   * ------------
   * The class has the following protected members, which means they are accessible to classes
   * which inherit this class: name (std::string), age (size_t), size (size_t), treasures 
   * (std::list<Treasure>).
   */


public:
  Dragon(std::string const& name, size_t age, size_t size);
  virtual ~Dragon() {}

  std::string const& getName() const;
  size_t getAge() const;
  size_t getSize() const;
  std::list<Treasure> const& getTreasures() const; 
  virtual void eat(std::list<Food> const&) {}
  virtual void hoard(std::list<Treasure> const&) {}


protected:
  std::string name;
  size_t age;
  size_t size;
  std::list<Treasure> treasures;

};



/* Description:
 * ------------
 * An overloaded << stream operator for printing Dragons to parameter ostreams.
 * The dragon should be printed to the parameter ostream in the following format (¶ means std::endl):
 *
 * Dragon named: <Dragon's name>, age: <Dragon's age>, size: <Dragon's size>¶
 * Treasures:¶
 * <First Treasure's name in the Dragon's treasures list>¶
 * <Second Treasure's name in the Dragon's treasures list>¶
 * .
 * .
 * .
 * <Last Treasure's name in the Dragon's treasures list>¶
 *
 * Parameters:
 * ------------
 * os: the output stream where the information about the Dragon is printed.
 * dragon: the Dragon which information should be printed.
 *
 * Returns:
 * ------------
 * The parameter output stream.
 *
 */
std::ostream& operator<<(std::ostream& os, Dragon const& dragon);

class DragonCave
{
  /* Description:
   * ------------
   * The DragonCave class is a sort of storage class for Dragons. The class
   * stores pointers of Dragon objects in a list. The class allows basic
   * manipulation of the Dragon pointer list through the functions described
   * below.
   *
   * Functions:
   * -----------
   * The DragonCave class has a parameterless constructor. 

   * The destructor of the DragonCave class deletes all the Dragons in the
   * the DragonCave's Dragon* list.
   *
   * getDragons: the function returns a const reference to the DragonCave's
   * list of Dragon pointers. The function takes no parameters and does not change 
   * the DragonCave's state, i.e. the function should be const.
   *
   * accommodateDragon: takes a Dragon* as a parameter and adds it to the end of
   * the DragonCave's Dragon pointer list. The function returns nothing.
   * 
   * evictDragon: takes a Dragon's name as a const string reference as a parameter.
   * Tries to find a dragon with a matching name from the DragonCaves Dragon pointer
   * list. Erases the first Dragon pointer with a matching name from the list. If
   * there is no Dragon with a matching name in the list the function does nothing.
   * Note that the function only erases the pointer from the list, but DOES NOT delete
   * the Dragon object. The function returns nothing.
   *
   * Other:
   * -----------
   * The class obviously has a list of Dragon* as a private member. This class is not
   * copyable (also means not assignable), so copy and assignment operations for this
   * class should be prevented.
   *
   */
    public:
    DragonCave();
    ~DragonCave();
    DragonCave(const DragonCave&) = delete; //disallow copying
    DragonCave &operator = (const DragonCave&) = delete; //disallow assingment

    std::list<Dragon*> const& getDragons() const;
    void accommodateDragon(Dragon* dragon);
    void evictDragon(std::string const& name);

    
  
    private:
    std::list<Dragon*> dragons;
};

/* Description:
 * ------------
 * An overloaded << stream operator for printing DragonCaves to parameter ostreams.
 * The DragonCave should be printed to the parameter ostream in the following format:
 *
 * DragonCave dwellers:¶
 * ¶
 * <First Dragon in the dragons list>
 * ¶
 * <Second Dragon in the dragons list>
 * ¶
 * .
 * .
 * .
 * ¶
 * <Last Dragon in the dragons list>
 *
 * NOTE: The dragons in the dragon list are printed in the same format as the overloaded
 *       << stream operator for Dragons prints them. Note the line changes between each 
 *       individual Dragon.
 *
 * Parameters:
 * ------------
 * os: the output stream where the information about the DragonCave is printed.
 * cave: the DragonCave which information should be printed.
 *
 * Returns:
 * ------------
 * The parameter output stream.
 */
std::ostream& operator<<(std::ostream& os, DragonCave const& cave);




class ChineseDragon : public Dragon
{
  public:
  
    /* Description:
     * ------------
     * Constructor of the ChineseDragon class.
     *
     * Parameters:
     * ------------
     * name: name of the dragon
     * age: age of the dragon
     * size: size of the dragon
     */
    ChineseDragon(std::string const& name, size_t age, size_t size);
  
    /* Description:
     * ------------
     * The eat function of the ChineseDragon class. ChineseDragons eat only Food which
     * type is either Herbs or PeopleFood. Each eaten Food item increases the
     * ChineseDragon's size by one.
     *
     * The food that the ChineseDragon eats is removed from the parameter food list and
     * each time the ChineseDragon feasts upon something you should print information of
     * the Food that is being eaten to std::cout in the following format:
     *
     * Chinese dragon ate: <Food's name>¶
     *
     * You should loop through the food list from beginning to end.
     *
     * Parameters:
     * ------------
     * food: a list of food containing possible edible items.
     *
     * Returns:
     * ------------
     * Nothing.
     */
    void eat(std::list<Food>& food);
    
    /* Description:
     * ------------
     * The hoard function of the ChineseDragon class. ChineseDragons hoard only Treasures which
     * type is Wisdom. Each hoarded Treasure is added to the ChineseDragon's treasures list
     * and is removed from the parameter treasures list.
     *
     * Each time the ChineseDragon hoards an item you should print information about the hoarded
     * item to std::cout in the following format:
     *
     * Chinese dragon received: <Treasure's name>¶
     *
     * You should loop through the treasures list from beginning to end.
     *
     * Parameters:
     * ------------
     * treasures: a list of treasures containing possible hoardable items.
     *
     * Returns:
     * ------------
     * Nothing.
     */
    void hoard(std::list<Treasure>& treasures);
};



class FantasyDragon : public Dragon
{
  public:
  
    /* Description:
     * ------------
     * Constructor of the FantasyDragon class.
     *
     * Parameters:
     * ------------
     * name: name of the dragon
     * age: age of the dragon
     * size: size of the dragon
     */
    FantasyDragon(std::string const& name, size_t age, size_t size);
  
    /* Description:
     * ------------
     * The eat function of the FantasyDragon class. FantasyDragons eat only Food which
     * type is either People or PeopleFood. Each eaten Food item increases the
     * FantasyDragon's size by one.
     *
     * The food that the FantasyDragon eats is removed from the parameter food list and
     * each time the FantasyDragon feasts upon something you should print information of
     * the Food that is being eaten to std::cout in the following format:
     *
     * Fantasy dragon ate: <Food's name>¶
     *
     * You should loop through the food list from beginning to end.
     *
     * Parameters:
     * ------------
     * food: a list of food containing possible edible items.
     *
     * Returns:
     * ------------
     * Nothing.
     */
    void eat(std::list<Food>& food);
    
    /* Description:
     * ------------
     * The hoard function of the FantasyDragon class. FantasyDragons hoard only Treasures which
     * type is Jewellery. Each hoarded Treasure is added to the FantasyDragon's treasures list
     * and is removed from the parameter treasures list.
     *
     * Each time the FantasyDragon hoards an item you should print information about the hoarded
     * item to std::cout in the following format:
     *
     * Fantasy dragon received: <Treasure's name>¶
     *
     * You should loop through the treasures list from beginning to end.
     *
     * Parameters:
     * ------------
     * treasures: a list of treasures containing possible hoardable items.
     *
     * Returns:
     * ------------
     * Nothing.
     */
    void hoard(std::list<Treasure>& treasures);
};



class MagicDragon : public Dragon
{
  public:
    MagicDragon(std::string const& name, size_t age, size_t size);
    
    void eat(std::list<Food>& food);
    void hoard(std::list<Treasure>& treasures);
};

#endif

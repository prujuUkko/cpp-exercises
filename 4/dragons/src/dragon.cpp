#include "dragon.hpp"

#include <string>
#include <list>
#include <iostream>

/* Dragon class functions here*/
Dragon::Dragon(std::string const& name, size_t age, size_t size): 
name(name), age(age), size(size), treasures() {}

std::string const& Dragon::getName() const {return name;}
size_t Dragon::getAge() const {return age;}
size_t Dragon::getSize() const {return size;}
std::list<Treasure> const& Dragon::getTreasures() const{return treasures;}

std::ostream& operator<<(std::ostream& os, Dragon const& dragon) {
  os<<"Dragon named: "<<dragon.getName()<<", age: "<<dragon.getAge()<<", size: "<<dragon.getSize()<<std::endl;
  os<<"Treasures:"<<std::endl;

  for (auto it : dragon.getTreasures()) {
    os<<it.name<<std::endl;
  }
  return os;
}

/* DragonCave class functions here*/
DragonCave::DragonCave(): dragons() {}
   
// Made for you, as it uses a tiny bit of dynamic memory here! You will see more of this in the next round.
DragonCave::~DragonCave() 
{ 
  for (std::list<Dragon*>::iterator it = dragons.begin(); it != dragons.end(); it++)
    delete *it;
}

std::list<Dragon*> const& DragonCave::getDragons() const {return dragons;}

void DragonCave::accommodateDragon(Dragon* dragon) {
  dragons.push_back(dragon);
}

void DragonCave::evictDragon(std::string const& name) {
  for (std::list<Dragon*>::iterator it = dragons.begin(); it != dragons.end();) {
    if ((*it)->getName() == name) {
      it = dragons.erase(it);
    }
    else it++;  
  }
}

std::ostream& operator<<(std::ostream& os, DragonCave const& cave) {
  os<<"DragonCave dwellers:"<<std::endl;
  
  for (auto it : cave.getDragons()) {
    
    os<<std::endl<<*it;
  }
  return os;
}

/* ChineseDragon class functions */
//Constructor here
ChineseDragon::ChineseDragon(std::string const& name, size_t age, size_t size):
Dragon(name, age, size) {}

void ChineseDragon::eat(std::list<Food>& food){
  for (std::list<Food>::iterator it = food.begin(); it != food.end();) {
    if (it->type == PeopleFood || it->type == Herbs) {
      std::cout<<"Chinese dragon ate: "<<it->name<<std::endl;
      it = food.erase(it);
      size++;
    }
    else it++;
  }
}

void ChineseDragon::hoard(std::list<Treasure>& treasures){
  for (std::list<Treasure>::iterator it = treasures.begin(); it != treasures.end();) {
    if (it->type == Wisdom) {
      std::cout<<"Chinese dragon received: "<<it->name<<std::endl;
      this->treasures.push_back(*it);
      it = treasures.erase(it);
    }
    else it++;
  }
}

/* FantasyDragon class functions */
//Constructor here
FantasyDragon::FantasyDragon(std::string const& name, size_t age, size_t size):
Dragon(name, age, size) {}

/* Fantasy dragon eats: People and PeopleFood */
void FantasyDragon::eat(std::list<Food>& food){
  for (std::list<Food>::iterator it = food.begin(); it != food.end();) {
    if (it->type == People || it->type == PeopleFood) {
      std::cout<<"Fantasy dragon ate: "<<it->name<<std::endl;
      it = food.erase(it);
      size++;
    }
    else it++;
  }
}

void FantasyDragon::hoard(std::list<Treasure>& treasures){
  for (std::list<Treasure>::iterator it = treasures.begin(); it != treasures.end();) {
    if (it->type == Jewellery) {
      std::cout<<"Fantasy dragon received: "<<it->name<<std::endl;
      this->treasures.push_back(*it);
      it = treasures.erase(it);
    }
    else it++;
  }
}
    

/* MagicDragon class functions */
MagicDragon::MagicDragon(std::string const& name, size_t age, size_t size) : Dragon(name, age, size) { }
    
void MagicDragon::eat(std::list<Food>& food)
{
  for (std::list<Food>::iterator it = food.begin(); it != food.end();)
    if (it->type == Herbs)
    {
      std::cout << "Magic dragon ate: " << it->name << std::endl;
      it = food.erase(it);
      size++;
    } else it++;
}

void MagicDragon::hoard(std::list<Treasure>& treasures)
{
  for (std::list<Treasure>::iterator it = treasures.begin(); it != treasures.end();)
    if (it->type == Potions)
    {
      std::cout << "Magic dragon received: " << it->name << std::endl;
      this->treasures.push_back(*it);
      it = treasures.erase(it);
    } else it++;
}
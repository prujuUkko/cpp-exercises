#include <iostream>
#include <vector>
#include "vector_it.hpp"

std::vector<int> readVector() {
    std::vector<int> numbers;

    while (1) {
        int read_number;
        std::cin >> read_number;
    
        if (std::cin.fail()) {
            return numbers;
        }    
        
        numbers.push_back (read_number);
    }
}

void printSum1(std::vector<int>& v) {
    int sum = 0;
    for (auto it = v.begin()+1; it != v.end(); it++) {
       sum = *(it-1) + *it;
       std::cout<<sum<<" ";
    }
    std::cout<<std::endl;
}

void printSum2(std::vector<int>& v) {
    auto it_1 = v.begin();
    int sum = 0;

    for (auto it_2 = (v.end() -1) ; it_2 > it_1; it_1++, it_2--) {
        sum = *it_1 + *it_2;
        std::cout<<sum<<" ";
    }
    
    std::cout<<std::endl;
}

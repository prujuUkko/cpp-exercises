#include "list.hpp"
#include <iostream>

/*
//http://www.cplusplus.com/reference/list/list/sort/
bool compare_nocase (const std::string& first, const std::string& second)
{
  unsigned int i=0;
  while ( (i<first.length()) && (i<second.length()) )
  {
    if (tolower(first[i])<tolower(second[i])) return true;
    else if (tolower(first[i])>tolower(second[i])) return false;
    ++i;
  }
  return ( first.length() < second.length() );
}
*/

std::istream& getLines(std::istream& is, std::list<std::string>& list) {
    while (1) {
        std::string line;
        std::getline(is, line);

        if (is.fail() || is.eof()) {
            return is;
        }

        list.push_back(line);
    }
    
}

void print(std::list<std::string> const& list) {
    for (auto it = list.begin(); it != list.end(); it++) {
        std::cout<<*it<<std::endl;
    }
}


void sortAndUnique(std::list<std::string>& list) {
    list.sort();
    list.unique();

}



#include <iostream>
#include <vector>
#include "matrix.hpp"
#include <algorithm>

// Remember to use auto and ranged for-loops when they can be used!
// Assumes 'm' is a square matrix
Matrix readMatrix(int n) {
    Matrix matrix_n;
    int number;
    
    for (int it_1 = 0; it_1 < n; it_1++) {
        std::vector<int> matrix_line;
        matrix_n.push_back(matrix_line);
        
        for (int it_2 = 0; it_2 < n; it_2++) {
            std::cin>>number;
            matrix_n[it_1].push_back(number);
            
        }
    }
    
    std::cout<<matrix_n[0][0]<<std::endl;
    return matrix_n;

}

Matrix rotate90deg(Matrix & m) {
   
   //New zero matrix of same size 
   Matrix new_m;
   for (unsigned int it_1 = 0; it_1 < m.size(); it_1++) {
        std::vector<int> matrix_line;
        new_m.push_back(matrix_line);
        
        for (unsigned int it_2 = 0; it_2 < m.size(); it_2++) {
            new_m[it_1].push_back(0);
            
        }
    }
   
   //Transpose then reverse rows, build new matrix into new_m
   for (unsigned int it_1 = 0; it_1 < m.size(); it_1++) {
       for (unsigned int it_2 = 0; it_2 < m[it_1].size(); it_2++) {
           new_m[it_1][it_2] = m[it_2][it_1]; //transpose
           std::reverse(new_m[it_1].begin(), new_m[it_1].end()); //reverse, found in <algorithm>
       }
   }

   return new_m;
}

void print(Matrix & m){
    std::cout << "Printing out a " << m.size() << " x " << m.size() << " matrix:" << std::endl;

    for (auto it_1 : m) {   
        for (auto it_2 : it_1) {
           std::cout<<it_2<<" ";
        }
        std::cout<<std::endl;
    }
}

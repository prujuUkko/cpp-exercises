#include <iostream>
#include "book.hpp"
#include "customer.hpp"
#include "library.hpp"


Library::Library(std::string const &Name) {
    name = Name;
    std::vector<Book> vb;
    std::vector<Customer> vc;
    books = vb;
    customers = vc;
}

std::string Library::getName() const {
    return name;
}

std::vector<Book> &Library::getBooks() {
    return books;
}

std::vector<Customer> &Library::getCustomers() {
    return customers;
}

Book Library::findBookByName(const std::string &book_name) {
    for (auto i : books) {
        if (i.getName() == book_name){
            return i;
        }   
    }

    Book not_found(" ", " ", " ");
    return not_found;
}

std::vector<Book> Library::findBooksByAuthor(const std::string &author_name) {
    std::vector<Book> found_books;
    for (auto i : books) {
        if (i.getAuthor() == author_name) {
            found_books.push_back(i);
        }
    }
    return found_books;
}        

std::vector<Book> Library::findAllLoanedBooks() {
    std::vector<Book> found_books;
    for (auto i : books) {
        if (i.getStatus()) {
            found_books.push_back(i);
        } 
    }
    return found_books;
}


Customer Library::findCustomer(const std::string &customer_id) {
    for (auto i : customers) {
        if (i.getID() == customer_id) {
            return i; 
        }
    }

    Customer not_found(" "," ");
    return not_found;
}